package io.polyakov.fabricintegration;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("hello-jni");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric f = new Fabric.Builder(this).debuggable(true).kits(new Crashlytics(),  new CrashlyticsNdk()).build();
        Fabric.with(f);

        setContentView(R.layout.activity_main);

        // Example of a call to a native method

    }

    public void forceCrash(View view) {
        final TextView tv = findViewById(R.id.sample_text);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tv.setText(new HelloJni().stringFromJNI());
            }
        }, 2000);
    }


//    /**
//     * A native method that is implemented by the 'native-lib' native library,
//     * which is packaged with this application.
//     */
//    public native String stringFromJNI();
}
